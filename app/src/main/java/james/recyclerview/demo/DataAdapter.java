package james.recyclerview.demo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.RowItem> {

    private static final int VIEW_TYPE_ODD = 1;
    private static final int VIEW_TYPE_EVEN = 2;

    private List<String> users;
    private Context context;

    public DataAdapter(Context context) {
        this.context = context;
    }

    //CREATE VIEW: onCreateView Fragment - initView - return View - use Inflater service - for rows
    @NonNull
    @Override
    public RowItem onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = null;

        switch (getItemViewType(i)) {
            case VIEW_TYPE_EVEN:
                view = inflater.inflate(R.layout.row_view_even, viewGroup, false);
                break;
            case VIEW_TYPE_ODD:
                view = inflater.inflate(R.layout.row_view_odd, viewGroup, false);
                break;
            default:
        }
        return new RowItem(view);
    }

    //BIND DATA FOR VIEW CREATED - Bind data to view - view.setText(...)
    @Override
    public void onBindViewHolder(@NonNull RowItem rowItem, int i) {
        String user = users.get(i);
        rowItem.rowNumber.setText(i + "");
        rowItem.rowContent.setText("ABC" + user);
    }


    //SET NUMBER ITEMS ON LIST
    @Override
    public int getItemCount() {
        return users == null ? 0 : users.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position % 2 == 0 ? VIEW_TYPE_EVEN : VIEW_TYPE_ODD;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    public class RowItem extends RecyclerView.ViewHolder {

        TextView rowNumber, rowContent;

        public RowItem(@NonNull View itemView) {
            super(itemView);
            rowContent = itemView.findViewById(R.id.row_content);
            rowNumber = itemView.findViewById(R.id.row_number);
        }
    }
}
